<?php

namespace Database\Seeders;

use App\User;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         User::factory()->create([
             'email' => 'contacto@lauchoit.com',
         ]);

        User::factory()->create([
            'email' => 'segundo@lauchoit.com',
        ]);

        User::factory()->create([
            'email' => 'tercero@lauchoit.com',
        ]);
    }
}
